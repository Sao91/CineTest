/**
 * 
 */
package com.CineTest.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.CineTest.bean.MovieBillboard;
import com.CineTest.bean.Response;
import com.CineTest.service.MoviebillboardService;
import com.CineTest.util.ResponseUtil;

/**
 * @author Ricardo Santos
 *
 */
@RestController
@RequestMapping("/rest")
public class MovieBillboardController {
	@Autowired
	private MoviebillboardService moviebillboardService;
	ResponseUtil response = new ResponseUtil();
	
	@RequestMapping(value = "/movieBillboardInsert", method = RequestMethod.POST)
	public Object movieInsert(@RequestBody MovieBillboard movieBillboard) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=moviebillboardService.movieBillboardInsert(movieBillboard);
			if (flag!=1) {
				resp=response.res("Error","/rest/movieBillboardInsert","500");
			} else {
				resp=response.res("Succes","/rest/movieBillboardInsert","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieBillboardInsert","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/movieBillboardDelete", method = RequestMethod.POST)
	public Object movieDelete(@RequestBody MovieBillboard movieBillboard) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=moviebillboardService.movieBillboardDelete(movieBillboard);
			if (flag==0) {
				resp=response.res("Error","/rest/movieBillboardDelete","500");
			} else {
				resp=response.res("Succes","/rest/movieBillboardDelete","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieBillboardDelete","500");
		}
		return resp;
	}
}

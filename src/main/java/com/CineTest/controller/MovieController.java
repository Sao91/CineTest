package com.CineTest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.CineTest.bean.Movie;
import com.CineTest.bean.Response;
import com.CineTest.service.MovieService;
import com.CineTest.util.ResponseUtil;

/**
 * @author Ricardo Santos
 *
 */
@RestController
@RequestMapping("/rest")
public class MovieController {
	@Autowired
	private MovieService movieService;
	ResponseUtil response = new ResponseUtil();
	
	@RequestMapping(value = "/movieAll", method = RequestMethod.POST)
	public Object movieAll() throws IOException {
		List<Movie> movie=null;
		Response resp = new Response();
		try {
			movie=movieService.movieAll();
			resp=response.res("Succes","/rest/movieAll","200",movie);
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieAll","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/movieId", method = RequestMethod.POST)
	public Object movieId(@RequestBody Movie movie) throws IOException {
		Response resp = new Response();
		try {
			movie=movieService.movieId(movie.getId());
			resp=response.res("Succes","/rest/movieId","200",movie);
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieId","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/movieInsert", method = RequestMethod.POST)
	public Object movieInsert(@RequestBody Movie movie) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=movieService.movieInsert(movie);
			if (flag!=1) {
				resp=response.res("Error","/rest/movieInsert","500");
			} else {
				resp=response.res("Succes","/rest/movieInsert","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieInsert","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/movieUpdate", method = RequestMethod.POST)
	public Object movieUpdate(@RequestBody Movie movie) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=movieService.movieUpdate(movie);
			if (flag!=1) {
				resp=response.res("Error","/rest/movieUpdate","500");
			} else {
				resp=response.res("Succes","/rest/movieUpdate","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieUpdate","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/movieDelete", method = RequestMethod.POST)
	public Object movieDelete(@RequestBody Movie movie) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=movieService.movieDelete(movie.getId());
			if (flag!=1) {
				resp=response.res("Error","/rest/movieDelete","500");
			} else {
				resp=response.res("Succes","/rest/movieDelete","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/movieDelete","500");
		}
		return resp;
	}

}

/**
 * 
 */
package com.CineTest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.CineTest.service.MovieService;
import com.CineTest.service.TurnService;

/**
 * @author Ricardo Santos
 *
 */
@Controller
public class WebController {
	@Autowired
	private MovieService movieService;
	
	@Autowired
	private TurnService turnService;
	
	//@Autowired TurnService turnService;

	@RequestMapping(value ={ "/", "/movies"})
	public String movie(Model m) {
		m.addAttribute("table",movieService.movieAll());
		System.out.println(movieService.movieAll().toString());
		return "web/movies";
	}

	@RequestMapping(value = "/turns")
	public String turn(Model m) {
		m.addAttribute("table",turnService.turnAll());
		System.out.println(turnService.turnAll().toString());
		return "web/turns";
	}
	
	
}

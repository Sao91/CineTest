/**
 * 
 */
package com.CineTest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.CineTest.bean.Response;
import com.CineTest.bean.Turn;
import com.CineTest.service.TurnService;
import com.CineTest.util.ResponseUtil;

/**
 * @author Ricardo Santos
 *
 */
@RestController
@RequestMapping("/rest")
public class TurnController {
	ResponseUtil response = new ResponseUtil();
	@Autowired
	private TurnService turnService;
	
	@RequestMapping(value = "/turnAll", method = RequestMethod.POST)
	public Object turnAll() throws IOException {
		List<Turn> turn=null;
		Response resp = new Response();
		try {
			turn=turnService.turnAll();
			resp=response.res("Succes","/rest/turnAll","200",turn);
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/turnAll","500");
			
		}
		return resp;
	}
	
	@RequestMapping(value = "/turnId", method = RequestMethod.POST)
	public Object turnId(@RequestBody Turn turn) throws IOException {
		Response resp = new Response();
		try {
			turn=turnService.turnId(turn.getId());
			resp=response.res("Succes","/rest/turnId","200",turn);
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/turnId","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/turnInsert", method = RequestMethod.POST)
	public Object turnInsert(@RequestBody Turn turn) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=turnService.turnInsert(turn);
			if (flag!=1) {
				resp=response.res("Error","/rest/turnInsert","500");
			} else {
				resp=response.res("Succes","/rest/turnInsert","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/turnInsert","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/turnUpdate", method = RequestMethod.POST)
	public Object turnUpdate(@RequestBody Turn turn) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=turnService.turnUpdate(turn);
			if (flag!=1) {
				resp=response.res("Error","/rest/turnUpdate","500");
			} else {
				resp=response.res("Succes","/rest/turnUpdate","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/turnUpdate","500");
		}
		return resp;
	}
	
	@RequestMapping(value = "/turnDelete", method = RequestMethod.POST)
	public Object turnDelete(@RequestBody Turn turn) throws IOException {
		Response resp = new Response();
		int flag=0;
		try {
			flag=turnService.turnDelete(turn.getId());
			if (flag!=1) {
				resp=response.res("Error","/rest/turnDelete","500");
			} else {
				resp=response.res("Succes","/rest/turnDelete","200");
			}
		} catch (Exception e) { 
			// TODO: handle exception
			e.printStackTrace();
			resp=response.res("Error",e.getMessage(),"/rest/turnDelete","500");
		}
		return resp;
	}

}

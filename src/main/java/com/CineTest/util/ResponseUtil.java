/**
 * 
 */
package com.CineTest.util;

import java.sql.Timestamp;

import com.CineTest.bean.Response;

/**
 * @author Ricardo Santos
 *
 */
public class ResponseUtil {
	
	public Response res( String message,String error, String path,String status, Object data) {
		Response response = new Response();
		response.setMessage(message);
		response.setTimestamp(new Timestamp(System.currentTimeMillis()));
		response.setError(error);
		response.setPath(path);
		response.setStatus(status);
		response.setData(data);
		return response;
	}
	
	public Response res( String message,String error, String path,String status) {
		Response response = new Response();
		response.setMessage(message);
		response.setTimestamp(new Timestamp(System.currentTimeMillis()));
		response.setError(error);
		response.setPath(path);
		response.setStatus(status);
		return response;
	}
	
	public Response res( String message,String path,String status) {
		Response response = new Response();
		response.setMessage(message);
		response.setTimestamp(new Timestamp(System.currentTimeMillis()));
		response.setPath(path);
		response.setStatus(status);
		return response;
	}
	
	public Response res( String message, String path,String status,Object data) {
		Response response = new Response();
		response.setMessage(message);
		response.setTimestamp(new Timestamp(System.currentTimeMillis()));
		response.setPath(path);
		response.setStatus(status);
		response.setData(data);
		return response;
	}
}

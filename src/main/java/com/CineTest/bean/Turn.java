/**
 * 
 */
package com.CineTest.bean;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Ricardo Santos
 *
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Turn implements Serializable{

	private long id;
		
	String time;
	
	private Integer status;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the publication_date
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param publication_date the publication_date to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "turns [id=" + id + ", time=" + time + ", status=" + status
				+ "]";
	}
	
}

/**
 * 
 */
package com.CineTest.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Ricardo Santos
 *
 */
public class MovieBillboard implements Serializable{

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		private long movie_id;
		
		private long turn_id;

		private List<Turn> turn;

		/**
		 * @return the movie_id
		 */
		public long getMovie_id() {
			return movie_id;
		}

		/**
		 * @param movie_id the movie_id to set
		 */
		public void setMovie_id(long movie_id) {
			this.movie_id = movie_id;
		}

		/**
		 * @return the turn_id
		 */
		public long getTurn_id() {
			return turn_id;
		}

		/**
		 * @param turn_id the turn_id to set
		 */
		public void setTurn_id(long turn_id) {
			this.turn_id = turn_id;
		}

		/**
		 * @return the turn
		 */
		public List<Turn> getTurn() {
			return turn;
		}

		/**
		 * @param turn the turn to set
		 */
		public void setTurn(List<Turn> turn) {
			this.turn = turn;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "MovieBillBoard [movie_id=" + movie_id + ", turn_id=" + turn_id + ", turn=" + turn + "]";
		}
		
}

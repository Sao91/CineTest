/**
 * 
 */
package com.CineTest.dao;

import java.util.List;

import com.CineTest.bean.Turn;

/**
 * @author Ricardo Santos
 *
 */
public interface TurnDao {
	
	public List<Turn> turnAll();
	
	public Turn turnId(long id);
	
	public int turnInsert(Turn turn);
	
	public int turnUpdate(Turn turn);
	
	public int turnDelete(long id);
}

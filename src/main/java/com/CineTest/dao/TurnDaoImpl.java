/**
 * 
 */
package com.CineTest.dao;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.CineTest.bean.Turn;

/**
 * @author Ricardo Santos
 *
 */
@Repository
public class TurnDaoImpl implements TurnDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.TurnDao#turnAll()
	 */
	@Override
	public List<Turn> turnAll() {
		// TODO Auto-generated method stub

		String sql = "SELECT *  FROM TURN "; 
		
		List<Turn> turnList = new ArrayList<Turn>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		for (Map row : rows) {
			Turn turn = new Turn();
			turn.setId((long) row.get("ID"));
			turn.setTime((String) row.get("TIME"));
			turn.setStatus((Integer) row.get("STATUS"));
			turnList.add(turn);
		}
		return turnList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.TurnDao#turnId(long)
	 */
	@Override
	public Turn turnId(long id) {
		// TODO Auto-generated method stub
		String sql = "SELECT *  FROM TURN WHERE ID=?"; 
		
		Turn turn = new Turn();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,id);
		for (Map row : rows) {
			turn.setId((long) row.get("ID"));
			turn.setTime((String) row.get("TIME"));
			turn.setStatus((Integer) row.get("STATUS"));	
		}
		return turn;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.TurnDao#MovieInsert(com.crudTest.bean.Turn)
	 */
	@Override
	@Transactional
	public int turnInsert(Turn turn) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "INSERT INTO TURN(TIME,STATUS) VALUES(?,?) ";
		try {
			resp=jdbcTemplate.update(sql,turn.getTime(),turn.getStatus());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.TurnDao#turnUpdate(com.crudTest.bean.Turn)
	 */
	@Override
	@Transactional
	public int turnUpdate(Turn turn) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "UPDATE TURN SET TIME=?, STATUS=? WHERE ID=? ";
		try {
			resp=jdbcTemplate.update(sql,turn.getTime(),turn.getStatus(),turn.getId());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.TurnDao#TurnDelete(long)
	 */
	@Override
	@Transactional
	public int turnDelete(long id) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "DELETE FROM TURN WHERE ID=? ";
		try {
			resp=jdbcTemplate.update(sql,id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}
}

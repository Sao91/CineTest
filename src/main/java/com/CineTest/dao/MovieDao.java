/**
 * 
 */
package com.CineTest.dao;

import java.util.List;

import com.CineTest.bean.Movie;

/**
 * @author Ricardo Santos
 *
 */
public interface MovieDao {
	
	public List<Movie> movieAll();
	
	public Movie movieId(long id);
	
	public int movieInsert(Movie movie);
	
	public int movieUpdate(Movie movie);
	
	public int movieDelete(long id);
}

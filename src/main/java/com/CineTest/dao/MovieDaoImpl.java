/**
 * 
 */
package com.CineTest.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.CineTest.bean.Movie;

/**
 * @author Ricardo Santos
 *
 */
@Repository
public class MovieDaoImpl implements MovieDao{
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.MovieTest.dao.MovieDao#MovieAll()
	 */
	@Override
	public List<Movie> movieAll() {
		// TODO Auto-generated method stub

		String sql = "SELECT *  FROM MOVIE ";
		
		List<Movie> movieList = new ArrayList<Movie>();
		try {
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
			for (Map row : rows) {
				Movie movie = new Movie();
				movie.setId((long) row.get("ID"));
				movie.setStatus((Integer)row.get("STATUS"));
				movie.setPublication_date((Date)row.get("PUBLICATION_DATE"));
				movie.setName((String)row.get("NAME"));
				movie.setImage((String)row.get("IMAGE"));
				movieList.add(movie);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return movieList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.MovieDao#MovieId(long)
	 */
	@Override
	public Movie movieId(long id) {
		// TODO Auto-generated method stub
		String sql = "SELECT *  FROM MOVIE WHERE ID=?";
		
		Movie movie = new Movie();
		try {
			List<Map<String, Object>> rows =  jdbcTemplate.queryForList(sql,new Object[] { id });
			for (Map row : rows) {
				movie.setId((long) row.get("ID"));
				movie.setStatus((Integer)row.get("STATUS"));
				movie.setPublication_date((Date)row.get("PUBLICATION_DATE"));
				movie.setName((String)row.get("NAME"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return movie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.MovieDao#MovieInsert(com.crudTest.bean.Movie)
	 */
	@Override
	@Transactional
	public int movieInsert(Movie movie) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "INSERT INTO MOVIE (NAME,PUBLICATION_DATE,STATUS,IMAGE) VALUES (?,?,?,?) ";
		try {
			resp=jdbcTemplate.update(sql,movie.getName(),movie.getPublication_date(),movie.getStatus(), movie.getImage());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.MovieDao#MovieUpdate(com.crudTest.bean.Movie)
	 */
	@Override
	@Transactional
	public int movieUpdate(Movie movie) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "UPDATE MOVIE SET NAME=?, STATUS=?,IMAGE=?,PUBLICATION_DATE=? WHERE ID=? ";
		try {
			resp=jdbcTemplate.update(sql,movie.getName(),movie.getStatus(), movie.getImage(),movie.getPublication_date(),movie.getId());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.crudTest.dao.MovieDao#MovieDelete(long)
	 */
	@Override
	@Transactional
	public int movieDelete(long id) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "DELETE FROM MOVIE WHERE ID=? ";
		try {
			resp=jdbcTemplate.update(sql,id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}
}

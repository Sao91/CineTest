/**
 * 
 */
package com.CineTest.dao;

import org.codehaus.groovy.ast.expr.EmptyExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.CineTest.bean.MovieBillboard;
import com.CineTest.bean.Turn;



/**
 * @author Ricardo Santos
 *
 */
@Repository
public class MovieBillBoardDaoImpl implements MovieBillboardDao{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	/* (non-Javadoc)
	 * @see com.CineTest.dao.MovieBillBoardDao#movieBillboardInsert(com.CineTest.controller.MovieBillboard)
	 */
	@Override
	@Transactional
	public int movieBillboardInsert(MovieBillboard movieBillboard) {
		// TODO Auto-generated method stub
		int resp=0;
		String sql = "INSERT INTO MOVIE_TURN (TURN_ID,MOVIE_ID) VALUES (?,?) ";
		try {
			for (Turn turn : movieBillboard.getTurn()) {
				resp=jdbcTemplate.update(sql,turn.getId(),movieBillboard.getMovie_id());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/* (non-Javadoc)
	 * @see com.CineTest.dao.MovieBillBoardDao#movieBillboardDelete(com.CineTest.controller.MovieBillboard)
	 */
	@Override
	@Transactional
	public int movieBillboardDelete(MovieBillboard movieBillboard) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			if(movieBillboard.getMovie_id()!=0) {
				String sql = "DELETE FROM MOVIE_TURN WHERE MOVIE_ID=? ";
				try {
					resp=jdbcTemplate.update(sql,movieBillboard.getMovie_id());
					System.out.println(resp);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}else if(movieBillboard.getTurn_id()!=0 ){
				String sql = "DELETE FROM MOVIE_TURN WHERE TURN_ID=? ";
				try {
					resp=jdbcTemplate.update(sql,movieBillboard.getTurn_id());
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	
		return resp;
	}

}

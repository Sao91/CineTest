/**
 * 
 */
package com.CineTest.dao;

import com.CineTest.bean.MovieBillboard;

/**
 * @author Ricardo Santos
 *
 */
public interface MovieBillboardDao {

	public int movieBillboardInsert(MovieBillboard movieBillboard);
	
	public int movieBillboardDelete(MovieBillboard movieBillboard);
}

/**
 * 
 */
package com.CineTest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CineTest.bean.MovieBillboard;
import com.CineTest.dao.MovieBillboardDao;

/**
 * @author Ricardo Santos
 *
 */
@Service
public class MoviebillboardServiceImpl implements MoviebillboardService{
	@Autowired
	private MovieBillboardDao movieBillBoardDao;

	/* (non-Javadoc)
	 * @see com.CineTest.service.MoviebillboardService#movieBillboardInsert(com.CineTest.bean.MovieBillBoard)
	 */
	@Override
	public int movieBillboardInsert(MovieBillboard movieBillboard) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			resp=movieBillBoardDao.movieBillboardInsert(movieBillboard);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/* (non-Javadoc)
	 * @see com.CineTest.service.MoviebillboardService#movieBillboardDelete(com.CineTest.bean.MovieBillBoard)
	 */
	@Override
	public int movieBillboardDelete(MovieBillboard movieBillboard) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			resp=movieBillBoardDao.movieBillboardDelete(movieBillboard);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}
	
}

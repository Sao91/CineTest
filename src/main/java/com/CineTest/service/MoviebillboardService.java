/**
 * 
 */
package com.CineTest.service;

import com.CineTest.bean.MovieBillboard;

/**
 * @author Ricardo Santos
 *
 */
public interface MoviebillboardService {
	
	public int movieBillboardInsert(MovieBillboard movieBillboard);
	
	public int movieBillboardDelete(MovieBillboard movieBillboard);
}

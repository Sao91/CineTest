/**
 * 
 */
package com.CineTest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CineTest.bean.Movie;
import com.CineTest.bean.MovieBillboard;
import com.CineTest.dao.MovieBillboardDao;
import com.CineTest.dao.MovieDao;

/**
 * @author Ricardo Santos
 *
 */
@Service
public class MovieServiceImpl implements MovieService {
	@Autowired
	private MovieDao movieDao;
	
	@Autowired
	private MovieBillboardDao movieBillboardDao;
	/* (non-Javadoc)
	 * @see com.crudTest.service.MovieService#movieAll()
	 */
	@Override
	public List<Movie> movieAll() {
		// TODO Auto-generated method stub
		List <Movie> movie = null;
		try {
			movie=movieDao.movieAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return movie;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.MovieService#movieId(long)
	 */
	@Override
	public Movie movieId(long id) {
		Movie movie = null;
		try {
			movie=movieDao.movieId(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return movie;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.MovieService#movieInsert(com.crudTest.bean.Movie)
	 */
	@Override
	public int movieInsert(Movie movie) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			resp=movieDao.movieInsert(movie);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.MovieService#movieUpdate(com.crudTest.bean.Movie)
	 */
	@Override
	public int movieUpdate(Movie turn) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			resp=movieDao.movieUpdate(turn);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.MovieService#movieDelete(long)
	 */
	@Override
	public int movieDelete(long id) {
		// TODO Auto-generated method stub
		int resp=0;
		MovieBillboard movieBillboard=new MovieBillboard();
		try {
			movieBillboard.setMovie_id(id);
			resp=movieBillboardDao.movieBillboardDelete(movieBillboard);
			resp=movieDao.movieDelete(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}
	
	
}

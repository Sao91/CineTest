/**
 * 
 */
package com.CineTest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CineTest.bean.MovieBillboard;
import com.CineTest.bean.Turn;
import com.CineTest.dao.MovieBillboardDao;
import com.CineTest.dao.TurnDao;

/**
 * @author Ricardo Santos
 *
 */
@Service
public class TurnServiceImpl implements TurnService {

	@Autowired
	private TurnDao turnDao;
	
	@Autowired
	private MovieBillboardDao movieBillboardDao;
	
	/* (non-Javadoc)
	 * @see com.crudTest.service.TurnService#turnAll()
	 */
	@Override
	public List<Turn> turnAll() {
		// TODO Auto-generated method stub
		List <Turn> turn = null;
		try {
			turn=turnDao.turnAll();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return turn;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.TurnService#turnId(long)
	 */
	@Override
	public Turn turnId(long id) {
		// TODO Auto-generated method stub
		Turn turn = null;
		try {
			turn=turnDao.turnId(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return turn;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.TurnService#turnInsert(com.crudTest.bean.Turn)
	 */
	@Override
	public int turnInsert(Turn turn) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			resp=turnDao.turnInsert(turn);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.TurnService#turnUpdate(com.crudTest.bean.Turn)
	 */
	@Override
	public int turnUpdate(Turn turn) {
		// TODO Auto-generated method stub
		int resp=0;
		try {
			resp=turnDao.turnUpdate(turn);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

	/* (non-Javadoc)
	 * @see com.crudTest.service.TurnService#turnDelete(long)
	 */
	@Override
	public int turnDelete(long id) {
		// TODO Auto-generated method stub
		int resp=0;
		MovieBillboard movieBillboard =new MovieBillboard();
		try {
			movieBillboard.setTurn_id(id);
			resp=movieBillboardDao.movieBillboardDelete(movieBillboard);
			resp=turnDao.turnDelete(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return resp;
	}

}

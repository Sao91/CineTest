/**
 * 
 */
package com.CineTest.service;

import java.util.List;

import com.CineTest.bean.Movie;

/**
 * @author Ricardo Santos
 *
 */
public interface MovieService {
	
	public List<Movie> movieAll();

	public Movie movieId(long id);

	public int movieInsert(Movie crud);

	public int movieUpdate(Movie crud);

	public int movieDelete(long id);
}
